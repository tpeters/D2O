# D2O
# Copyright (C) 2016  Theo Steininger
#
# Author: Theo Steininger
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from .config import configuration as gc,\
                    dependency_injector as gdi

MPI = gdi[gc['mpi_module']]

custom_MIN = MPI.Op.Create(lambda x, y, datatype:
                           np.amin(np.vstack((x, y)), axis=0)
                           if isinstance(x, np.ndarray) else
                           min(x, y))

custom_MAX = MPI.Op.Create(lambda x, y, datatype:
                           np.amax(np.vstack((x, y)), axis=0)
                           if isinstance(x, np.ndarray) else
                           max(x, y))

custom_NANMIN = MPI.Op.Create(lambda x, y, datatype:
                              np.nanmin(np.vstack((x, y)), axis=0))

custom_NANMAX = MPI.Op.Create(lambda x, y, datatype:
                              np.nanmax(np.vstack((x, y)), axis=0))

custom_UNIQUE = MPI.Op.Create(lambda x, y, datatype:
                              np.unique(np.concatenate([x, y])))

op_translate_dict = {}

# the value tuple contains the operator and a boolean which specifies
# if the operator is compatible to buffers (for Allreduce instead of allreduce)
op_translate_dict[np.sum] = (MPI.SUM, True)
op_translate_dict[np.prod] = (MPI.PROD, True)
op_translate_dict[np.amin] = (custom_MIN, False)
op_translate_dict[np.amax] = (custom_MAX, False)
op_translate_dict[np.all] = (MPI.BAND, True)
op_translate_dict[np.any] = (MPI.BOR, True)
op_translate_dict[np.nanmin] = (custom_NANMIN, False)
op_translate_dict[np.nanmax] = (custom_NANMAX, False)
op_translate_dict[np.unique] = (custom_UNIQUE, False)
