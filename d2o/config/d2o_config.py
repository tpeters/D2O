# D2O
# Copyright (C) 2016  Theo Steininger
#
# Author: Theo Steininger
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import keepers

# Setup the dependency injector
dependency_injector = keepers.DependencyInjector(
                                    ['h5py',
                                     ('mpi4py.MPI', 'MPI'),
                                     ('mpi_dummy', 'MPI_dummy')]
                                     )

dependency_injector.register('pyfftw', lambda z: hasattr(z, 'FFTW_MPI'))


# Initialize the variables
variable_mpi_module = keepers.Variable('mpi_module',
                                       ['MPI', 'MPI_dummy'],
                                       lambda z: z in dependency_injector)

variable_default_distribution_strategy = keepers.Variable(
                            'default_distribution_strategy',
                            ['fftw', 'equal', 'not'],
                            lambda z: (('pyfftw' in dependency_injector)
                                       if (z == 'fftw') else True)
                                                  )

variable_mpi_init_checks = keepers.Variable('mpi_init_checks',
                                            [True, False],
                                            lambda z: isinstance(z, bool),
                                            genus='boolean')

# Construct the configuration object
configuration = keepers.get_Configuration(name='D2O', file_name='D2O.conf')


[configuration.register(vari) for vari in
    [variable_mpi_module,
     variable_default_distribution_strategy,
     variable_mpi_init_checks]]


# register the default comm variable as the 'mpi_module' variable is now
# available
variable_default_comm = keepers.Variable(
                     'default_comm',
                     ['COMM_WORLD'],
                     lambda z: hasattr(dependency_injector[
                                       configuration['mpi_module']], z))

configuration.register(variable_default_comm)

try:
    configuration.load()
except:
    pass
